<?php

namespace Alumn\Classes;

class TerrainLeveling
{
    /**
     * Length of area elements
     *
     * @var int null
     */
    private $length = null;

    /**
     * Store all level values from the whole area
     * @var array
     */
    private $areaValues = [];

    /**
     * Return minimum effort to level an area
     *
     * @param []string $area array of level in each square meter
     *
     * @return int
     * @throws \OutOfRangeException
     */
    public function getMinimum(array $area)
    {
        try {
            $this->validateArea($area);
        }
        catch (\OutOfRangeException $e) {
                return $e->getMessage();
        }

        // If the difference of all elements in area is only 1 or there is no area, the area is already leveled
        if (empty($this->areaValues) || (max($this->areaValues) - min($this->areaValues) <= 1)) {
            return 0;
        }

        $efforts = [];
        // Check all possibilities of optimal values, as we have only 9 digits, it's easier to test all possibilities than manipulate arrays
        for ($i = 0; $i < 9; $i++) {
            $effort = 0;
            foreach($this->areaValues as $areaLevel)
            {
                // If level is superior to the bigger optimal value
                if (intval($areaLevel) >= ($i+1)) {
                    $effort += (intval($areaLevel) - ($i+1));
                } else {
                    // If level is inferior to the lower optimal value
                    $effort += ($i - intval($areaLevel));
                }
            }
            $efforts[] = $effort;
        }

        return min($efforts);
    }

    /**
     * Check if element of an array is numeric and length < 50
     *
     * @param mixed $n element of an array
     * @throws \OutOfRangeException
     */
    private function checkValue($n)
    {
        if (strlen($n) > 50 || !is_numeric($n) || preg_match("/\./", $n)) {
            throw new \OutOfRangeException("Error - Length too long, should be < 50 digits or value is not numeric nor integer: " . $n);
        }

        $currentElementLength = strlen($n);
        if (is_null($this->length)) {
            $this->length = $currentElementLength;
        } else if ($this->length != $currentElementLength) {
            throw new \OutOfRangeException("Error - Elements of the area are not the same length: " . $n);
        }

        // Merge current area level with already parsed one
        $this->areaValues = array_merge($this->areaValues, preg_split('//', $n, -1, PREG_SPLIT_NO_EMPTY));
    }

    /**
     * Verify if all data are valid
     *
     * @param array $area
     * @throws \OutOfRangeException
     */
    private function validateArea(array $area)
    {
        if (count($area) > 50) {
            throw new \OutOfRangeException("Error - Maximum area lines is 50");
        }
        // Check if length is between 1 and 9 and the same for each element
        // Check if each area is between 0 - 9
        \array_map(array($this, "checkValue"), $area);
    }
}
