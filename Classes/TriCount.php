<?php


namespace Alumn\Classes;


class TriCount
{
    const MAX_LENGTH = 1000000;

    const MAX_POSSIBILITIES = 1000000000;

    /**
     * Count all combinations of triangle in the range of $minLength and $maxLength border size
     *
     * @param int $minLength
     * @param int $maxLength
     * @return mixed string|int
     */
    public function count($minLength, $maxLength)
    {
        // Test the unwanted values
        if (!is_numeric($minLength) || $minLength < 1 || $minLength > self::MAX_LENGTH) {
            return 0;
        }
        else if (!is_numeric($maxLength) || $maxLength < $minLength || $maxLength > self::MAX_LENGTH) {
            return 0;
        }

        // Parse all possibilities with 3 loop, third value will increment
        // Then second, then first one to avoid duplicated combinations
        // TODO replace by the maths formula that count in range
        $combinations = 0;
        for ($i = $minLength; $i <= $maxLength; $i++) {
            for ($j = $i; $j <= $maxLength; $j++) {
                for ($k = $j; $k <= $maxLength; $k++) {
                    if ($i + $j > $k) {
                        $combinations++;
                        if ($combinations > self::MAX_POSSIBILITIES) {
                            return -1;
                        }
                    }
                }
            }
        }

        return $combinations;
    }
}