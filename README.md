Code exercices
========

Problem 1
---------

**Problem Statement:**

You are building a new house. You have already purchased a rectangular area where you will
place the house. The basement of the house should be built on a level ground, so you will have
to level the entire area. The area is leveled if the difference between the heights of its lowest
and highest square meter is at most 1. You want to measure the effort you need to put into
ground leveling. You are given a string[] area. Each character in area denotes the height at the
corresponding square meter of the terrain area. Using 1 unit of effort, you can change the height
of any square meter on his area by 1 up or down. Return the minimum total effort you need to
put to obtain a leveled area.

Definition

Class:
TerrainLeveling
Method:
getMinimum
Parameters:
string[]
Returns:
int
Method signature:
int getMinimum(string[] area)

Constraints

* area will contain between 1 and 50 elements, inclusive.
* Each element of area will be between 1 and 50 characters long, inclusive.
* All elements of area will be of the same length.
* Each element of area will contain digits ('0'­'9') only.

Examples

1. {"10",
"31"}
Returns: 2
The given area is not leveled, because the minimum height is 0 and the maximum height
is 3. You need to reduce the height of lower left square by 2.
2. {"54454",
"61551"}
Returns: 7
In the optimal solution each square will have height either 4 or 5. To reach such a
configuration, you should reduce the height of one square from 6 to 5, and increase the
heights of two other squares from 1 to 4.
3. {"989"}
Returns: 0
The area is already leveled.
4. {"90"}
Returns: 8
5. {"5781252",
"2471255",
"0000291",
"1212489"}
Returns: 53


**Problem Statement:**

We are interested in triangles that have integer length sides, all of which are between
minLength and maxLength, inclusive. How many such triangles are there?
Two triangles differ if they have a different collection of side lengths, ignoring order. Triangles
with side lengths {2,3,4} and {4,3,5} differ, but {2,3,4} and {4,2,3} do not. We are only interested
in proper triangles; the sum of the two smallest sides of a proper triangle must be strictly greater
than the length of the biggest side.
Create a class TriCount that contains a method count that is given ints minLength and
maxLength and returns the number of different proper triangles whose sides all have lengths
between minLength and maxLength, inclusive. If there are more than 1,000,000,000 return ­1.

Definition

Class:
TriCount
Method:
count
Parameters:
int, int
Returns:
int
Method signature:
int count(int minLength, int maxLength)

Constraints

* minLength is between 1 and 1,000,000, inclusive.
* maxLength is between minLength and 1,000,000, inclusive.

Examples

1. 1
2
Returns: 3
 The proper triangles with side lengths between 1 and 2 inclusive are {1,1,1} and {2,2,2}
 and {1,2,2}.
 
2. 9
10
Returns: 4
9,9,9 and 10,10,10 and 9,9,10 and 9,10,10
3. 1
1000000
Returns: ­1
There are VERY many triangles with lengths in this range.
4. 19
1000
Returns: 83540657