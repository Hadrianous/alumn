<?php

namespace Tests;

require('../Classes/TerrainLeveling.php');

use Alumn\Classes\TerrainLeveling;

class TerrainLevelingTest extends \PHPUnit_Framework_TestCase
{
    public function badAreaValues()
    {
        return [
            [ //dataset 0
                [
                    "123a"
                ]
            ],
            [ //dataset 1
                [
                    "12,3"
                ]
            ],
            [ //dataset 2
                [
                    "12.3"
                ]
            ],
            [ //dataset 3
                [
                    "123452149875632541208930320178456985201297032145098"
                ]
            ],
            [ //dataset 4
                [
                    "1",
                    "26"
                ]
            ],
        ];
    }
    /**
     * @dataProvider badAreaValues
     */
    public function testLevelingErrors($values)
    {
        $terrainLeveling = new TerrainLeveling();
        $effort = $terrainLeveling->getMinimum($values);
        $this->assertRegexp("/Error -/", $effort);
    }

    public function testLevelingErrors51Values()
    {
        $values = [];
        for ($i=0; $i<51; $i++) {
            $values[] = "8521";
        }
        $terrainLeveling = new TerrainLeveling();
        $effort = $terrainLeveling->getMinimum($values);
        $this->assertRegexp("/Error -/", $effort);
    }

    public function testLeveling50IsOk()
    {
        $values = [];
        for ($i=0; $i<50; $i++) {
            $values[] = "8521";
        }
        $terrainLeveling = new TerrainLeveling();
        $effort = $terrainLeveling->getMinimum($values);
        $this->assertInternalType("int", $effort);

        $terrainLeveling = new TerrainLeveling();
        $effort = $terrainLeveling->getMinimum(["12345214987563254120893032017845698520129703214501"]);
        $this->assertInternalType("int", $effort);
    }


    public function areaValues()
    {
        return [
            [ // dataset 0
                [
                    "10",
                    "31"
                ],
                2
            ],
            [ // dataset 1
                [
                    "989"
                ],
                0
            ],
            [ // dataset 2
                [
                    "898"
                ],
                0
            ],
            [ // dataset 3
                [
                    "90"
                ],
                8
            ],
            [ // dataset 4
                [
                    "54454",
                    "61551"
                ],
                7
            ],
            [ // dataset 5
                [
                    "5781252",
                    "2471255",
                    "0000291",
                    "1212489"
                ],
                53
            ],
            [ // dataset 6
                [
                    "0"
                ],
                0
            ],
            [ // dataset 7
                [
                    "9"
                ],
                0
            ],
            [ // dataset 8
                [
                    "9999"
                ],
                0
            ],
            [ // dataset 9
                [
                    "789"
                ],
                1
            ],
            [ // dataset 10
                [
                    "789",
                    "789"
                ],
                2
            ],
        ];
    }

    /**
     * @dataProvider areaValues
     */
    public function testLeveling($values, $expectedResult)
    {
        $terrainLeveling = new TerrainLeveling();
        $effort = $terrainLeveling->getMinimum($values);
        $this->assertEquals($expectedResult, $effort);
    }
}