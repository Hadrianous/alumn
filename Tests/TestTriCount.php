<?php


namespace Tests;

require('../Classes/TriCount.php');

use Alumn\Classes\TriCount;

class TestTriCount extends \PHPUnit_Framework_TestCase
{
    public function triangleValues()
    {
        return [
            [
                [
                    1, 2
                ],
                3,
            ],
            [
                [
                    9,
                    10
                ],
                4
            ],
            [
                [
                    1,
                    1000000
                ],
                -1
            ],
            [
                [
                    19,
                    1000
                ],
                83540657
            ],
            [
                [
                    1000,
                    1000001
                ],
                0
            ],
            [
                [
                    1000001,
                    1000001
                ],
                0
            ],
            [ // Lowest length is superior to higher length
                [
                    1000,
                    10
                ],
                0
            ],
            [
                [
                    "10k",
                    "11k"
                ],
                0
            ],
        ];
    }

    /**
     * @dataProvider triangleValues
     */
    public function testTriCount($values, $expectedResult)
    {
        $triCount = new TriCount();
        $trianglesCount = $triCount->count($values[0], $values[1]);
        $this->assertEquals($expectedResult, $trianglesCount);
    }
}